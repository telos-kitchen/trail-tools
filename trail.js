const commandLineArgs = require("command-line-args");
const { Api, JsonRpc } = require("eosjs");
const fetch = require("node-fetch"); // node only; not needed in browsers
require ("dotenv/config");
const { TextEncoder, TextDecoder } = require("util");
const { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig");
var moment = require('moment');
moment().utc().format();

async function execute (actions) {
  const signatureProvider = new JsSignatureProvider([process.env.PRIVATE_KEY]);
  const rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch } );

  const api = new Api({
    rpc,
    signatureProvider,
    textDecoder: new TextDecoder(),
    textEncoder: new TextEncoder()
  });

  const result = await api.transact({ actions: actions }, { blocksBehind: 3, expireSeconds: 30 });
  console.log (JSON.stringify(result, null, 2));
  return result;
}

async function createPoll (ballotName, ballotTitle, ballotDescription, ballotContent, account, symbol, options) {
  const actions = [
    {
      account: "trailservice",
      name: "newballot",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        ballot_name: ballotName,
        category: "poll",
        publisher: account,
        treasury_symbol: symbol,
        voting_method: "1acct1vote",
        initial_options: options
      }
    },{
      account: "trailservice",
      name: "togglebal",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        ballot_name: ballotName,
        setting_name: "votestake"
      }
    },{
      account: "trailservice",
      name: "editdetails",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        ballot_name: ballotName,
        title: ballotTitle,
        description: ballotDescription,
        content: ballotContent
      }
    },{
      account: "trailservice",
      name: "openvoting",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        ballot_name: ballotName,
        end_time: moment().utc().add(1, "d").format("YYYY-MM-DDTHH:mm:ss").toString()
      }
    }

  ];
  return await execute (actions);
}

async function createCommunity (manager, supply) {
  const actions = [
    {
      account: "trailservice",
      name: "newtreasury",
      authorization: [
        {
          actor: manager,
          permission: "active"
        }
      ],
      data: {
        manager: manager,
        max_supply: supply,
        access: "public"  // only support public for now
      }
    }
  ];
  return await execute (actions);
}

async function regvoter (account, symbol) {
  const actions = [
    {
      account: "trailservice",
      name: "regvoter",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        voter: account,
        treasury_symbol: symbol,
        referrer: ''
      }
    }
  ];
  console.log (JSON.stringify(actions))
  return await execute (actions);
}

async function vote (account, ballot, options) {
  const actions = [
    {
      account: "trailservice",
      name: "castvote",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        voter: account,
        ballot_name: ballot,
        options: options
      }
    }
  ];
  return await execute (actions);
}

async function printCommunities (comm) {
  let rpc;
  let options = {};

  rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch });
  options.code = "trailservice";
  options.scope = "trailservice";

  options.json = true;
  options.table = "treasuries";
  // options.index_position = 2;
  // options.key_type = 'i64';
  if (comm != "all") {
    options.lower_bound = comm; //"0";
    options.upper_bound = comm;  // to retrieve only that rank, set upper equal to lower
  }

  rpc.get_table_rows(options).then( result => {
    if (result.rows.length > 0) {
      console.log (JSON.stringify(result.rows, null, 2));
    } else {
      console.log ("There are no communities with symbol: ", comm);
    }
  })
}

async function printBallots (ballot) {
  let rpc;
  let options = {};

  rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch });
  options.code = "trailservice";
  options.scope = "trailservice";

  options.json = true;
  options.table = "ballots";
  if (ballot != "all") {
    options.lower_bound = ballot; //"0";
    options.upper_bound = ballot;  // to retrieve only that rank, set upper equal to lower
  }

  rpc.get_table_rows(options).then( result => {
    if (result.rows.length > 0) {
      console.log (JSON.stringify(result.rows, null, 2));
    } else {
      console.log ("There are no ballots with the name: ", ballot);
    }
  })
}

async function loadOptions() {
    const optionDefinitions = [
      { name: "action", alias: "a", type: String },
      { name: "comm", alias: "c", type: String},
      { name: "ballot", alias: "b", type: String},
      { name: "symbol", alias: "s", type: String },
      { name: "account", alias: "v", type: String },
      { name: "supply", alias: "y", type: String },
      { name: "title", type: String },
      { name: "description", type: String },
      { name: "content", type: String }, 
      { name: "options", type: String }
    ];
  
    return commandLineArgs(optionDefinitions);
}
  
async function main() {
  try {
    const opts = await loadOptions();
    console.log(opts);

    let actionFound = false;
    switch (opts.action) {

      case "query":
        actionFound = true;
        if (opts.comm) {
          await printCommunities (opts.comm);
        } else if (opts.ballot) {
          await printBallots (opts.ballot);
        }
        break;

      case "create":
        if (opts.ballot) {
          await createPoll (opts.ballot, opts.title, opts.description, opts.content, opts.account, opts.supply, opts.options)
        } else if (opts.supply) {
          await createCommunity (opts.account, opts.supply);
        } 
        actionFound = true;
        break;

      case "regvoter":
        await regvoter (opts.account, opts.symbol)
        actionFound = true;
        break;

      case "vote":
        await vote (opts.account, opts.ballot, opts.options )
        actionFound = true;
        break;
    }

    if (!actionFound) {
      console.log ("Action not recognized.");
    }
  } catch (e) {
    console.log (e);
    console.log ("Error occurred. ", JSON.stringify(e));
  }
}
  
main();