Private key: 5K77ptftc2U2kGzNRASrFXq8SZ382TJEtLw7mALh4NPXhWzhay5
Public key: EOS72ebkEHMv8MYWMGGfvPQJvGMnbLE3CZfJu2YCjHW4UuQjDHvz4

# create account
cleos -u https://test.telos.kitchen system newaccount --stake-net "1.0000 TLOS" --stake-cpu "1.0000 TLOS" --buy-ram "1.0000 TLOS" teloskitchen trailuser112 EOS72ebkEHMv8MYWMGGfvPQJvGMnbLE3CZfJu2YCjHW4UuQjDHvz4

# transfer tokens to trailservice
cleos -u https://test.telos.kitchen push action eosio.token transfer '["trailuser111", "trailservice", "1000.0000 TLOS", "deposit"]' -p trailuser111
cleos -u https://test.telos.kitchen push action eosio.token transfer '["trailuser112", "trailservice", "1000.0000 TLOS", "deposit"]' -p trailuser112
cleos -u https://test.telos.kitchen push action eosio.token transfer '["trailuser113", "trailservice", "1000.0000 TLOS", "deposit"]' -p trailuser113


# create a treasury/community
cleos -u https://test.telos.kitchen push action trailservice newtreasury '["trailuser111", "1000000.00 NYCTUG", "public"]' -p trailuser111

# register a voter
cleos -u https://test.telos.kitchen push action trailservice regvoter '["trailuser112", "2,NYCTUG", null]' -p trailuser112

# send voter some tokens
cleos -u https://test.telos.kitchen push action trailservice mint '["cehraphaim11", "10.00 NYCTUG", "original mint"]' -p trailuser111

# create a ballot
cleos -u https://test.telos.kitchen push action trailservice newballot '["eventloc1", "poll", "trailuser112", "2,NYCTUG", "1acct1vote", ["centralpark","empirebldg","brooklyn"]]' -p trailuser112
cleos -u https://test.telos.kitchen push action trailservice togglebal '["eventloc1", "votestake"]' -p trailuser112
cleos -u https://test.telos.kitchen push action trailservice editdetails '["eventloc1", "Where should we hold the event?", "There are many options of places where we can hold the events. Select the place that you prefer.", ""]' -p trailuser112
cleos -u https://test.telos.kitchen push action trailservice openvoting '["eventloc1", "2019-10-30T13:00:00"]' -p trailuser112

# cast vote
cleos -u https://test.telos.kitchen push action trailservice castvote '["trailuser112", "eventloc1", ["centralpark"]]' -p trailuser112

cleos -u https://test.telos.kitchen get table trailservice trailservice ballots

cleos -u https://test.telos.kitchen push action trailservice cancelballot '["eventloc", "messed up"]' -p trailuser112

cleos -u https://test.telos.kitchen push action trailservice 
cleos -u https://test.telos.kitchen push action trailservice 
cleos -u https://test.telos.kitchen get account trailuser111