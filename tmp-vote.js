const { Api, JsonRpc } = require("eosjs");
const commandLineArgs = require("command-line-args");
const fetch = require("node-fetch"); // node only; not needed in browsers
require ("dotenv/config");
const { TextEncoder, TextDecoder } = require("util");
const { JsSignatureProvider } = require("eosjs/dist/eosjs-jssig");

async function execute (actions) {
  const signatureProvider = new JsSignatureProvider([process.env.PRIVATE_KEY]);
  const rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch } );

  const api = new Api({
    rpc,
    signatureProvider,
    textDecoder: new TextDecoder(),
    textEncoder: new TextEncoder()
  });

  const result = await api.transact({ actions: actions }, { blocksBehind: 3, expireSeconds: 30 });
  console.log (JSON.stringify(result, null, 2));
  return result;
}

async function isRegistered (account, symbol) {
  let rpc;
  let options = {};

  rpc = new JsonRpc(process.env.EOSIO_ENDPOINT, { fetch });
  options.code = "trailservice";
  options.table = "voters";
  options.scope = account;
  options.json = true;
  options.lower_bound = symbol; 
  options.upper_bound = symbol;  
  
  rpc.get_table_rows(options).then( result => {
    if (result.rows.length > 0) {
      console.log (JSON.stringify(result.rows, null, 2));
      return true;
    } else {
      console.log ("Voter: ", account, " is not registered.");
      return false;
    }
  })
}

async function regandvote (account, symbol, ballot, options) {
  const actions = [
    {
      account: "trailservice",
      name: "regvoter",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        voter: account,
        treasury_symbol: symbol,
        referrer: ''
      }
    },{
      account: "trailservice",
      name: "castvote",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        voter: account,
        ballot_name: ballot,
        options: options
      }
    }
  ];
  console.log (JSON.stringify(actions))
  return await execute (actions);
}  

async function vote (account, ballot, options) {
  const actions = [
    {
      account: "trailservice",
      name: "castvote",
      authorization: [
        {
          actor: account,
          permission: "active"
        }
      ],
      data: {
        voter: account,
        ballot_name: ballot,
        options: options
      }
    }
  ];
  return await execute (actions);
}

async function loadOptions() {
  const optionDefinitions = [
    { name: "symbol", alias: "s", type: String },
    { name: "account", alias: "a", type: String },
    { name: "ballot", alias: "b", type: String },
    { name: "option", alias: "o", type: String }
  ];

  return commandLineArgs(optionDefinitions);
}

async function main () {
  try {
    const opts = await loadOptions();
    console.log(opts);

    if (!isRegistered(opts.account, opts.symbol)) {
      await regandvote (opts.account, opts.symbol, opts.ballot, opts.option);
    } else {
      await vote (opts.account, opts.ballot, opts.option);
    }

  } catch (e) {
    console.log (e);
  }
}

main ();
